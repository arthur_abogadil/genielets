var TableData = function () {
    //function to initiate DataTable
    //DataTable is a highly flexible tool, based upon the foundations of progressive enhancement, 
    //which will add advanced interaction controls to any HTML table
    //For more information, please visit https://datatables.net/
    var runDataTable = function () {
        
        var oTable = jQuery('#sample_1').dataTable({

            "aoColumnDefs": [{
                "aTargets": [0]
            }],
            "oLanguage": {
                "sLengthMenu": "Show _MENU_ Rows",
                "sSearch": "",
                "oPaginate": {
                    "sPrevious": "",
                    "sNext": ""
                }
            },
            "aaSorting": [
                [1, 'asc']
            ],
            "aLengthMenu": [
                [12, 10, 15, 20, -1],
                [12, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 12,

           


        });






        jQuery('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
        // modify table search input
        jQuery('#sample_1_wrapper .dataTables_length select').addClass("m-wrap small");
        // modify table per page dropdown
        jQuery('#sample_1_wrapper .dataTables_length select').select2();
        // initialzie select2 dropdown
        jQuery('#sample_1_column_toggler input[type="checkbox"]').change(function () {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });


        var oTable2 = jQuery('#sample_2').dataTable({

            "aoColumnDefs": [{
                "aTargets": [0]
            }],
            "oLanguage": {
                "sLengthMenu": "Show _MENU_ Rows",
                "sSearch": "",
                "oPaginate": {
                    "sPrevious": "",
                    "sNext": ""
                }
            },
            "aaSorting": [
                [1, 'asc']
            ],
            "aLengthMenu": [
                [12, 10, 15, 20, -1],
                [12, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 12,

           


        });






        jQuery('#sample_2_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
        // modify table search input
        jQuery('#sample_2_wrapper .dataTables_length select').addClass("m-wrap small");
        // modify table per page dropdown
        jQuery('#sample_2_wrapper .dataTables_length select').select2();
        // initialzie select2 dropdown
        jQuery('#sample_2_column_toggler input[type="checkbox"]').change(function () {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable2.fnSettings().aoColumns[iCol].bVisible;
            oTable2.fnSetColumnVis(iCol, (bVis ? false : true));
        });



    };
    return {
        //main function to initiate template pages
        init: function () {
            runDataTable();


        }
    };




}();