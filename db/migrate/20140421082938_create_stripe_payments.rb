class CreateStripePayments < ActiveRecord::Migration
  def change
    create_table :stripe_payments do |t|
      t.references :landlord, index: true
      t.date :payment_date
      t.string :subscription_type
      t.string :token
      t.string :email
      t.integer :amount
      t.text :notes

      t.timestamps
    end
  end
end
