class AddFilledToLandlord < ActiveRecord::Migration
  def change
    add_column :landlords, :filled, :boolean
  end
end
