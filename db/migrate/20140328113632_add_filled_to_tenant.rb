class AddFilledToTenant < ActiveRecord::Migration
  def change
    add_column :tenants, :filled, :boolean
  end
end
