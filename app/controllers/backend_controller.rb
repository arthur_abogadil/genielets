class BackendController < ApplicationController


  before_filter :authenticate_user!

  layout "backend"

  def payment_confirm
      @payment = Payment.where(:return_token => params[:id]).first
      @payment.status = "Paid"
      @payment.payment_method = "Paypal"
      @payment.date_paid = Date.today
      @payment.save
      redirect_to :action => 'contract_view', :id => @payment.id
  end

  def index
  end

  def tenant_list
   @connections = Connection.where(landlord_id: get_landlord.id, approved: true) 
  end

  def tenant_view
   

   @a = Connection.where(landlord_id: get_landlord.id, tenant_id: params[:id], approved: true) 
  
   if @a.count == 0
     redirect_to action: 'display_404'
   else
     @tenant = Tenant.find params[:id] 
   end 


  end

  def contracts_list

      if get_type == "Landlord"
        @contracts = Contract.where(landlord_id: get_landlord.id, active:true)
      end

      if get_type == "Tenant"
        @contracts = Contract.where(tenant_id: get_profile.id, active:true)
      end

  end

  def contract_edit

    if get_type == "Tenant"
                      redirect_to :controller => 'backend', :action => 'contract_view', :id => params[:id]
    end

      if get_type == "Landlord"

         @contract = Contract.new
         @tenants = Connection.where(landlord_id: get_landlord.id)
         @properties = Property.where(landlord_id: get_landlord.id)


         @contract = Contract.where(landlord_id: get_landlord.id, id: params[:id]).first

             if request.request_method_symbol == :patch
              @cont = Contract.find params[:id]
              if @cont.update_attributes(contract_params)
               
                @cont.landlord_id = get_landlord.id
                @cont.active = true
                @cont.save
                flash[:notice] = 'Contract Updated'
                redirect_to :controller => 'backend', :action => 'contract_view', :id => params[:id]

              else
                flash[:notice] = 'Error'
              end
            end

        @contracts = Contract.where(landlord_id: get_landlord.id)

      end

  end


  def contract_view

      if get_type == "Landlord"
        
        @contract = Contract.where(landlord_id: get_landlord.id, id: params[:id]).first
        @payments = Payment.where(contract_id: @contract.id)
        @payments_late = Payment.all(:conditions => ["due_date <= ? AND status = ? AND contract_id = ?",Date.today, 'Unpaid', @contract.id])
      
      end

      if get_type == "Tenant"
        
        @contract = Contract.where(tenant_id: get_profile.id, active:true).first
        @payments = Payment.where(contract_id: @contract.id)
        @payments_late = Payment.all(:conditions => ["due_date <= ? AND status = ? AND contract_id = ?",Date.today, 'Unpaid', @contract.id])

      end

     

  end

  def contracts_inactive

      if get_type == "Landlord"
        @contracts = Contract.where(landlord_id: get_landlord.id, active: false)
      end

      if get_type == "Tenant"
        @contracts = Contract.where(tenant_id: get_tenant.id, active: false)
      end

  end
  
  def contract_delete


    @contract = Contract.find params[:id]

    Payment.delete_all(contract_id: @contract.id, landlord_id: get_landlord.id)

    @contract.delete

    flash[:notice] = 'Contract Deleted!'
    redirect_to action: 'contracts_list'

  end

  def payment_edit

    if get_type != 'Landlord'
      redirect_to action: 'contracts_list'
    end 

    @payment = Payment.find(params[:id])

    if @payment.landlord_id != get_landlord.id 
      redirect_to action: 'contracts_list'
    end

    if request.request_method_symbol == :patch
     
      if @payment.update_attributes(payment_params)
        flash[:notice] = 'Payment Updated'
        if @payment.status == 'Unpaid'
          @payment.date_paid = nil
        end
        @payment.save

        redirect_to :controller => 'backend', :action => 'contract_view', :id => @payment.contract_id
      else
        flash[:notice] = 'Error'
      end

    end

  end

  def payment_params
    params.require(:payment).permit(:tenant_id, :property_id, :landlord_id, :contract_id, :status, :due_date, :late_payment, :date_paid, :reference_no, :payment_method, :amount)
  end



  def mark_as_paid
    
    if get_type != 'Landlord'

      redirect_to :controller => 'backend', :action => 'contract_view', :id => @payment.contract.id

    end

    @payment = Payment.find params[:id]

    if @payment.landlord_id = get_landlord.id 
      @payment.status = 'Paid'
      @payment.date_paid = Date.today
      @payment.save
    end

    redirect_to :controller => 'backend', :action => 'contract_view', :id => @payment.contract.id

  end

  def contracts_add

   @active_contracts_count = Contract.where(:landlord_id => get_landlord.id, :active => true ).count 

    if session[:package] == "Standard" or session[:package] == "Trial"
      
      if @active_contracts_count >= 5 

        flash[:notice] = 'Error: Standard and Trial Accounts are limited to 5 active contracts, please upgrade your account to Pro or Enterprise to add more contracts!'
        redirect_to action: "contracts_list"

        return 

      end

    end 

    if session[:package] == "Pro"
      
      if @active_contracts_count >= 10 

        flash[:notice] = 'Error: Pro accounts are limited to 10 active contracts, please upgrade to Enterprise account to add more contracts!'
        redirect_to action: "contracts_list"

        return 

      end

   end 
  
  
   @contract = Contract.new
   @tenants = Connection.where(landlord_id: get_landlord.id)
   @properties = Property.where(landlord_id: get_landlord.id)

   if request.request_method_symbol == :post
     
      @cont = Contract.new

      if @cont.update_attributes(contract_params)
       
        @cont.landlord_id = get_landlord.id
        @cont.active = true

        @cont.save
        generate_payments(@cont)

        flash[:notice] = 'Contract Added'
        redirect_to :controller => 'backend', :action => 'contract_view', :id => @cont.id

      else

        flash[:notice] = 'Error, Please Enter Values For All Important Fields, e.g. Rental Amount, etc...'

      end

    end

    @contracts = Contract.where(landlord_id: get_landlord.id)

  end

  def generate_payments(contract)

    #(date2.year * 12 + date2.month) - (date1.year * 12 + date1.month)

    num_of_months = (contract.end_date.year * 12 + contract.end_date.month) - (contract.start_date.year * 12 + contract.start_date.month)

    num_of_months.times do |n|
      
      calc_date = contract.start_date + (n + 1).months

      #get max days in the month, and use it if pay_date is greater than max days (e.g. 31, when in february there's only 28)
      days = Time.days_in_month(calc_date.month, calc_date.year)
      if contract.pay_date > days 
       calc_date = calc_date.change(day: days)
      else
        calc_date = calc_date.change(day: contract.pay_date)  
      end

      payment = Payment.new
      payment.tenant_id = contract.tenant.id
      payment.contract_id = contract.id
      payment.amount = contract.rental_amount
      payment.property_id = contract.property.id
      payment.landlord_id = contract.landlord.id
      payment.status = 'Unpaid'
      payment.due_date = calc_date
      payment.late_payment = false
      payment.date_paid = nil
      payment.return_token = SecureRandom.urlsafe_base64(16)

      payment.save

    end

  end

  def contract_params
    params.require(:contract).permit(:tenant_id, :property_id, :room_no, :rental_amount, :start_date, :end_date, :pay_date, :notes)
  end

  def add_tenant
    
   @connection = Connection.new

   if request.request_method_symbol == :post
      @conn = Connection.new
      if @conn.update_attributes(connection_params)
        flash[:notice] = 'Connection Request Added'
        @conn.landlord_id = get_landlord.id
        @conn.approved = false
        @conn.save
      else
        flash[:notice] = 'Error'

      end

   end

   @connections = Connection.where(landlord_id: get_landlord.id, approved: false) 

  end

  def connection_params
    params.require(:connection).permit(:tenant_email)
  end


  ####TENANT####
  def tenant_dashboard

    get_type

    @profile = get_profile

    #if no profile
    if !@profile 

      flash[:notice] = 'Please Enter Profile Data Before Continuing...'
      redirect_to action: 'tenant_profile'
      return

    end

    if @profile.filled == true
    time = Time.new
    @current_month = time.strftime("%B, %Y")
    @current_year =  time.strftime("%Y")
    @as_of = time.strftime("%d %B %Y")

    @date = Date.today

    @rent_payment = Payment.all(:conditions => ["due_date <= ? AND status = ? AND tenant_id = ?",Date.today, 'Unpaid', @profile.id])
    @rent_payment_needed = Payment.all(:conditions => ["due_date <= ? AND status = ? AND tenant_id = ?",Date.today, 'Unpaid', @profile.id]).count


    @rent_payments_made = Payment.all(:conditions => ["status = ? AND tenant_id = ?",'Paid', @profile.id]).count
    @late_payments_made = Payment.all(:conditions => ["status = ? AND tenant_id = ? AND late_payment = ?",'Paid', @profile.id, 'True']).count

    @connections = Connection.where(tenant_email: current_user.email, approved: false) 
  else
      flash[:notice] = 'Please Enter Profile Data Before Continuing...'
      redirect_to action: 'tenant_profile'
      return
  end

  end

  def tenant_profile

    @tenant = get_profile

    if request.request_method_symbol == :patch 
      if @tenant.update_attributes(tenant_profile_params)
        flash[:notice] = 'Profile Updated'
        @tenant.filled = true
        @tenant.save
      else
      end
    end 

    if !@tenant

      @tenant = Tenant.new 
      @tenant.user_id = getuser.id
      @tenant.filled = false
      @tenant.save

    end

  end


  def tenant_connection_requests
       @connections = Connection.where(tenant_email: current_user.email, approved: false) 

  end

  def active_connections
       @connections = Connection.where(tenant_email: current_user.email, approved: true) 
  end

 def approve_connection

       @conn = Connection.find params[:id]
       @conn.approved = true
       @conn.tenant_id = get_profile.id

       @conn.save

       flash[:notice] = 'You are now connected to ' + @conn.landlord.first_name + ' ' + @conn.landlord.last_name

       redirect_to action: 'active_connections'

  end

  def delete_connection

       @conn = Connection.find params[:id]
       @conn.delete

       flash[:notice] = 'Connection / Connection Request Deleted'

       if get_type == "Tenant"
        redirect_to action: 'active_connections'
       end
       
       if get_type == "Landlord"
        redirect_to action: 'add_tenant'
       end 


  end


  ####END TENANT#####
  def redirector

    if get_type == "Tenant"
      redirect_to action: 'tenant_dashboard'
      return
    end

    if get_type == "Landlord"
      redirect_to action: 'dashboard'
      return
    end

    if get_type == "Admin"
      redirect_to action: 'dashboard', controller: 'admins'
      return
    end






    @profile = get_profile
    get_subscription_type

    if !@profile 

      flash[:notice] = 'Please Enter Profile Data Before Continuing...'
      redirect_to action: 'profile'

    end

    

  end

  def check
   
      if get_profile == nil 
        return
      end

      if get_profile.subscription_id == ""
        return
      end

      @sub = Subscription.find get_profile.subscription_id
      #trial expired
      if @sub.subscription_type == "Trial" and @sub.date_to < Date.today

        redirect_to controller: 'backend', action: 'subscribe'   

      end

      #subscription expired
      if @sub.subscription_type != "Trial" and @sub.date_to < Date.today

        redirect_to controller: 'backend', action: 'renew'
        return   

      end 

      if @sub.payment_status == "Unpaid" 
        redirect_to controller: 'backend', action: 'subscribe'
      end


  end


  def dashboard

  	if get_type == "Tenant"
      redirect_to action: 'tenant_dashboard'
      return
    end

    @profile = get_profile
    get_subscription_type

    #if no profile
    if !@profile 

      flash[:notice] = 'Please Enter Profile Data Before Continuing...'
      redirect_to action: 'profile'
      return

    end

    if @profile.filled == true
    time = Time.new
    @current_month = time.strftime("%B, %Y")
    @current_year =  time.strftime("%Y")
    @as_of = time.strftime("%d %B %Y")

    #stats
    @property_count = Property.where(:landlord_id => @profile.id).count
    @active_contract_count = Contract.where(:landlord_id => @profile.id, :active => true).count;

    @months_payment_list = Payment.where(:landlord_id => @profile.id, :status => 'Paid', :date_paid => Date.today.beginning_of_month..Date.today.end_of_month)
    @months_payment = @months_payment_list.count
    @months_unpaid_list = Payment.where(:landlord_id => @profile.id, :status => 'Unpaid', :due_date => Date.today.beginning_of_month..Date.today.end_of_month)
    @months_unpaid  = @months_unpaid_list.count

    @contracts_starting = Contract.where(:landlord_id => @profile.id, :start_date => Date.today.beginning_of_month..Date.today.end_of_month)
    @contracts_ending = Contract.where(:landlord_id => @profile.id, :end_date => Date.today.beginning_of_month..Date.today.end_of_month)

    @sales_jan = 0
    @sales_feb = 0
    @sales_mar = 0
    @sales_apr = 0
    @sales_may = 0
    @sales_jun = 0
    @sales_jul = 0
    @sales_aug = 0
    @sales_sep = 0
    @sales_oct = 0
    @sales_nov = 0
    @sales_dec = 0

    @sales_jan = get_sum_rent_current_year(1)
    @sales_feb = get_sum_rent_current_year(2)
    @sales_mar = get_sum_rent_current_year(3)
    @sales_apr = get_sum_rent_current_year(4)
    @sales_may = get_sum_rent_current_year(5)
    @sales_jun = get_sum_rent_current_year(6)
    @sales_jul = get_sum_rent_current_year(7)
    @sales_aug = get_sum_rent_current_year(8)
    @sales_sep = get_sum_rent_current_year(9)
    @sales_oct = get_sum_rent_current_year(10)
    @sales_nov = get_sum_rent_current_year(11)
    @sales_dec = get_sum_rent_current_year(12)

    @unpaid_jan = 0
    @unpaid_feb = 0
    @unpaid_mar = 0
    @unpaid_apr = 0
    @unpaid_may = 0
    @unpaid_jun = 0
    @unpaid_jul = 0
    @unpaid_aug = 0
    @unpaid_sep = 0
    @unpaid_oct = 0
    @unpaid_nov = 0
    @unpaid_dec = 0

    @unpaid_jan = get_sum_rent_current_year_unpaid(1)
    @unpaid_feb = get_sum_rent_current_year_unpaid(2)
    @unpaid_mar = get_sum_rent_current_year_unpaid(3)
    @unpaid_apr = get_sum_rent_current_year_unpaid(4)
    @unpaid_may = get_sum_rent_current_year_unpaid(5)
    @unpaid_jun = get_sum_rent_current_year_unpaid(6)
    @unpaid_jul = get_sum_rent_current_year_unpaid(7)
    @unpaid_aug = get_sum_rent_current_year_unpaid(8)
    @unpaid_sep = get_sum_rent_current_year_unpaid(9)
    @unpaid_oct = get_sum_rent_current_year_unpaid(10)
    @unpaid_nov = get_sum_rent_current_year_unpaid(11)
    @unpaid_dec = get_sum_rent_current_year_unpaid(12)

   

    @paid_total =  @sales_jan + @sales_feb + @sales_mar + @sales_apr + @sales_may  + @sales_jun + @sales_jul + @sales_aug + @sales_sep + @sales_oct + @sales_nov + @sales_dec
    @unpaid_total =  @unpaid_jan + @unpaid_feb + @unpaid_mar + @unpaid_apr + @unpaid_may  + @unpaid_jun + @unpaid_jul + @unpaid_aug + @unpaid_sep + @unpaid_oct + @unpaid_nov + @unpaid_dec

    @currency = @profile.currency.upcase
  else
        flash[:notice] = 'Please Enter Profile Data Before Continuing...'
      redirect_to action: 'profile'
      return

  end

  end

  def get_sum_rent_current_year(month)
    
    @profile = get_profile
    @d = ("01-" + month.to_s + "-" + Date.today.year.to_s).to_date
    Payment.where(:landlord_id => @profile.id, :status => 'Paid', :date_paid => @d.beginning_of_month..@d.end_of_month).sum('amount')

  end

   def get_sum_rent_current_year_unpaid(month)
    
    @profile = get_profile
    @d = ("01-" + month.to_s + "-" + Date.today.year.to_s).to_date
    Payment.where(:landlord_id => @profile.id, :status => 'Unpaid', :due_date => @d.beginning_of_month..@d.end_of_month).sum('amount')

  end

  def account_summary
    
    get_type
    @landlord = get_landlord
    get_subscription_type

    @payment = Subscription.new

    
  end

  def renew

    @landlord = get_landlord
    @payment = Subscription.new

  end

   def subscribe

    @landlord = get_landlord
    @payment = Subscription.new

  end


  def charges

      # Amount in cents
      @amount = params[:hmt]
      if @amount == '300'
        @type = "Standard"
      elsif @amount == '500'
        @type = "Pro"
      elsif @amount == '1000'
        @type = "Enterprise"
      end

      Stripe.api_key = ENV['SECRET_KEY']
      customer = Stripe::Customer.create(
        :email => params[:stripeEmail],
        :card  => params[:stripeToken]
      )

      charge = Stripe::Charge.create(
        :customer    => customer.id,
        :amount      => @amount,
        :description =>  @type + " Package, Genielets Subscription",
        :currency    => 'gbp'
      )

      @landlord = get_landlord
      @old_subs = Subscription.find @landlord.subscription_id

      @subscription = Subscription.new
      @subscription.subscription_type = @type

      #new account with no subscription
      if @old_subs == nil
         @subscription.date_from = Date.today
        @subscription.date_to = Date.today + (1).months
      else
        @subscription.date_from = @old_subs.date_to
        @subscription.date_to = @old_subs.date_to + (1).months
      end
      
      @subscription.save

      @landlord.subscription_id = @subscription.id
      @landlord.subscription_type = @type

      @landlord.save
      @subscription.landlord_id = @landlord.id
      @subscription.save

      @amount = params[:hmt]

      if @amount == '300'
        @amount = '3.00'
      elsif @amount == '500'
        @amount = '5.00'
      elsif @amount == '1000'
        @amount = '10.00'
      end

      @stripepayment = StripePayment.new
      @stripepayment.payment_date = Date.today
      @stripepayment.subscription_type = @type
      @stripepayment.token = params[:stripeToken]
      @stripepayment.landlord_id = @landlord.id
      @stripepayment.email = params[:stripeEmail]
      @stripepayment.amount = @amount

      @stripepayment.save

      get_subscription_type

      @email = params[:stripeEmail] 
     
      rescue Stripe::CardError => e
        flash[:error] = e.message
        redirect_to :action => 'account_summary'

  end

  def profile
  	
  	get_type
  	
  	@landlord = get_landlord

    if request.request_method_symbol == :patch or request.request_method_symbol == :put
      if @landlord.update_attributes(profile_params)
        flash[:notice] = 'Profile Updated'
        @landlord.filled = true
        @landlord.save
      else
        flash[:notice] = 'Profile Update Error'
      end
    end  


    if !@landlord

      @landlord = Landlord.new 
      @landlord.user_id = getuser.id
      @landlord.filled = false
      @landlord.country = 'United States'
      @landlord.save

      sub_type = "Trial"

      @subscription = Subscription.new
      @subscription.subscription_type = sub_type
      @subscription.date_from = Date.today
      @subscription.date_to = Date.today + (1).months
      @subscription.save

      @landlord.subscription_id = @subscription.id
      @landlord.subscription_type = sub_type

      @landlord.save
      @subscription.landlord_id = @landlord.id
      @subscription.save

    end


  end

  def update_profile

    @landlord = get_landlord

  end

  #helpers
  #UTILITIES
  def get_profile
    
    if get_type == "Landlord"
      return Landlord.find_by_user_id(getuser.id)
    end

    if get_type == "Tenant"
      return Tenant.find_by_user_id(getuser.id)
    end
  
  end  

  def landlord_view
    @landlord = Landlord.find params[:id]
  end

  def getuser

    return User.find_by_email(current_user.email)

  end

  def get_landlord

    @l = Landlord.find_by_user_id(getuser.id)
    return @l

  end

  def get_subscription_type
    
    if get_landlord == nil
      return
    end

    check

    session[:subscription_type] = get_landlord.subscription_type 

  end      


  def get_tenant

    return Tenant.find_by_user_id(getuser.id)

  end

  def get_type

    session[:user_type] = User.find_by_email(current_user.email).user_type
    return session[:user_type]

  end

  def getproperties
    return Property.where(landlord_id: get_landlord.id)
  end

  def profile_params
    params.require(:landlord).permit(:avatar, :first_name, :middle_name, :last_name, :address_line_1, :address_line_2, :city_town, :province_state_county_region, :country, :primary_phone, :secondary_phone, :vat_number, :paypal_email_id, :bank_account_no, :bank_name, :bank_branch, :currency, :company_name, :company_description )
  end

  def tenant_profile_params
    params.require(:tenant).permit(:avatar, :first_name, :middle_name, :last_name, :sex, :dob, :primary_phone, :secondary_phone, :primary_email, :secondary_email, :notes)
  end

  def property_params
    params.require(:property).permit(:property_type, :number_of_rooms, :name_of_building, :address_line_1, :address_line_2, :city_town, :province_state_county_region, :country, :post_code, :phone_number, :approx_rental, :notes, :property_id)
  end

  def add_property
      
    if [:post, :patch].include?(request.request_method_symbol) 

      if request.request_method_symbol == :patch
         id = params[:property_id]
         @property = Property.find id

      else   
        
         @property = Property.new
         @property.landlord_id = get_landlord.id
     
      end 

      if @property.update_attributes(property_params)
        flash[:notice] = 'Property Added / Updated'
        redirect_to action: 'list_property'

        return
      else
        return
        flash[:notice] = 'Error'
      end
    end  

    @property = Property.new 
    @property_id = -1
    
      
  end



  def list_property

    @properties = getproperties()

  end


  def view_property

        @property = Property.find params[:id]

  end

   def delete_property

        Property.destroy params[:id]
        
        flash[:notice] = 'Property Deleted!'
        redirect_to action: 'list_property'


  end

  def edit_property
    
    @property = Property.find params[:id]
    @property_id = params[:id]

  end
  
  def payment_dues

    if get_type == "Landlord"
       @dues =  Payment.where("landlord_id = ? AND due_date < ? AND STATUS = 'Unpaid'", get_profile.id, Date.today)

    end

    if get_type == "Tenant"
          @dues =  Payment.where("tenant_id = ? AND due_date < ? AND STATUS = 'Unpaid'", get_profile.id, Date.today)
    end

  end

  
  def paid_month()

    #paid month, current year

    @date = ("01-" + params[:month].to_s + "-" + Date.today.year.to_s).to_date

    @dues = Payment.where(:landlord_id => get_profile.id, :status => 'Paid', :date_paid => @date.beginning_of_month..@date.end_of_month)
    time = Time.new
    @current_month = time.strftime("%B, %Y")

    @selected_month = @date.strftime("%B, %Y")

  end

   def unpaid_month()

    #paid month, current year

    @date = ("01-" + params[:month].to_s + "-" + Date.today.year.to_s).to_date

    @dues = Payment.where(:landlord_id => get_profile.id, :status => 'Unpaid', :due_date => @date.beginning_of_month..@date.end_of_month)
    time = Time.new
    @current_month = time.strftime("%B, %Y")

    @selected_month = @date.strftime("%B, %Y")

  end


  def paid_current_month

    @dues = Payment.where(:landlord_id => get_profile.id, :status => 'Paid', :date_paid => Date.today.beginning_of_month..Date.today.end_of_month)
    time = Time.new
    @current_month = time.strftime("%B, %Y")

  end

  def unpaid_current_month

    @dues = Payment.where(:landlord_id => get_profile.id, :status => 'Unpaid', :due_date => Date.today.beginning_of_month..Date.today.end_of_month)
    time = Time.new
    @current_month = time.strftime("%B, %Y")

  end



  def paid_dues

    

    if get_type == "Landlord"
       @dues =  Payment.where("landlord_id = ? AND STATUS = 'Paid'", get_profile.id)

    end

    if get_type == "Tenant"
          @dues =  Payment.where("tenant_id = ? AND STATUS = 'Paid'", get_profile.id)
    end


  end


  def payments_late

    

    if get_type == "Landlord"
       @dues =  Payment.where("landlord_id = ? AND STATUS = 'Paid' and late_payment = true", get_profile.id)

    end

    if get_type == "Tenant"
          @dues =  Payment.where("tenant_id = ? AND STATUS = 'Paid' and late_payment = true", get_profile.id)
    end


  end

  def search()

  end

    
  ##### ADMIN ########


  def admin

  end


  ##### END ADMIN ####




end
