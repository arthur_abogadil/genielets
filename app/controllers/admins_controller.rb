class AdminsController < ApplicationController

	layout "admins"


	def dashboard

		@landlords = Landlord.all.count
		@tenants = Tenant.all.count
		@trial = Landlord.where(:subscription_type => "Trial").count
		@standard = Landlord.where(:subscription_type => "Standard").count
		@pro = Landlord.where(:subscription_type => "Pro").count
		@enterprise = Landlord.where(:subscription_type => "Enterprise").count

		@didntlogin60 = User.all(:conditions => ["last_sign_in_at <= ? and user_type = ?", Date.today - 60.days, "Tenants"]).count

		#Landlords with out Properties
		@list = Property.select(:landlord_id).group(:landlord_id).map {|i| i.landlord_id}
		@noproperty = Landlord.where.not(id: @list).count

		#Landlords with trial expired but did not subscribe 
		@listwithsubs = Subscription.select(:landlord_id, :subscription_type).where.not(:subscription_type => "Trial").map {|i| i.landlord_id}
		@listwithexpiredtrial = Subscription.select(:landlord_id, :subscription_type).where("subscription_type = 'Trial' and date_to < ?", Date.today).map {|i| i.landlord_id}
		@nosubscription = Landlord.where(id: @listwithexpiredtrial).where.not(id: @listwithsubs).count

		#Tenant without Connection
		@withconnection = Connection.select(:tenant_id).map{|i| i.tenant_id}
		@noconnection = Tenant.where.not(id: @withconnection).count

		@signups = User.count(:group => 'date(created_at)').take(60)
		@rentpayments = Payment.where(:status => 'Paid').count(:group => 'date(created_at)').take(60)
		@subscriptionpayments = StripePayment.count(:group => 'date(created_at)').take(60)
	end


	def contracts

		@contracts = Contract.all

	end

	def contracts_view
	   
		@contract = Contract.where(id: params[:id]).first
		@payments = Payment.where(contract_id: @contract.id)
		@payments_late = Payment.all(:conditions => ["due_date <= ? AND status = ? AND contract_id = ?",Date.today, 'Unpaid', @contract.id])

	end

	def properties

		@properties = Property.all

	end

	def properties_view

		@property = Property.find params[:id]

	end

	def landlords

		@type = params[:type]
		if @type.nil?
			@landlords = Landlord.all
		else 
			@landlords = Landlord.where(:subscription_type => @type)
		end
			

	end

	def landlords_view

		@landlord = Landlord.find params[:id]
		@subscriptionpayments = StripePayment.where(:landlord_id => @landlord.id)


	end

	def tenants

		@tenants = Tenant.all

	end

	def tenants_view

		@tenant = Tenant.find params[:id]
		@rentpayments = Payment.where(:tenant_id => @tenant.id)


	end

	def tenants60

		@tenants = User.all(:include => :tenant, :conditions => ["last_sign_in_at <= ? and user_type = ?", Date.today - 60.days, "Tenant"])

	end

	def noproperty
		@list = Property.select(:landlord_id).group(:landlord_id).map {|i| i.landlord_id}
		@landlords = Landlord.where.not(id: @list)
	end

	def nosubscription
		#Landlords with trial expired but did not subscribe 
		@listwithsubs = Subscription.select(:landlord_id, :subscription_type).where.not(:subscription_type => "Trial").map {|i| i.landlord_id}
		@listwithexpiredtrial = Subscription.select(:landlord_id, :subscription_type).where("subscription_type = 'Trial' and date_to < ?", Date.today).map {|i| i.landlord_id}
		@landlords = Landlord.where(id: @listwithexpiredtrial).where.not(id: @listwithsubs)

	end

	def noconnection
		@withconnection = Connection.select(:tenant_id).map{|i| i.tenant_id}
		@tenants = Tenant.where.not(id: @withconnection)
	end

	def rentpayments
		@rentpayments = Payment.where(:status => 'Paid')
	end

	def subscriptionpayments
		@subscriptionpayments = StripePayment.all
	end



end
