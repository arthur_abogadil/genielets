class RegistrationsController < Devise::RegistrationsController

  before_filter :configure_permitted_parameters, :only => [:create]


  def new

 		session[:type] = params[:type]
    session[:package] = params[:package]
		super

  end

  def create

    super
  end

  def update
    super
  end

end 